Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ipe-tools
Upstream-Contact: Otfried Cheong <otfried@ipe.airpost.net>
Source: https://github.com/otfried/ipe-tools

Files: *
Copyright: 1993-2008, Otfried Cheong <otfried@ipe.airpost.net>
           2007-2015, Alexander Bürger <acfb@users.sf.net>
License: GPL-2+

Files: ipe5toxml/*
Copyright: 2005, Otfried Cheong <otfried@ipe.airpost.net>
           2015, Alexander Bürger <acfb@users.sf.net>
License: public-domain
 While the source code of svgtoipe does not include any licensing
 information, the homepage of the ipe tool,
 http://ipe7.sourceforge.net/, includes a hyperlink to the ipe-tools
 github repository and says:
  ``The source to ipe5toxml is in the public domain.''

Files: pdftoipe/*
Copyright: 2003-2014, Otfried Cheong <otfried@ipe.airpost.net>
License: GPL-2
 See /usr/share/common-licenses/GPL-2
Comment:
 the pdftoipe source code itself is GPL-2+, but it is linked
 to GPL-2 only libpoppler so it must be GPL-2, too

Files: svgtoipe/*
Copyright: 2009-2014, Otfried Cheong <otfried@ipe.airpost.net>
License: GPL-3+
 See /usr/share/common-licenses/GPL-3

Files: matplotlib/*
Copyright: 2014, Otfried Cheong <otfried@ipe.airpost.net>
           2014, Soyeon Baek
License: SimpleXMLWriterLicense
 By obtaining, using, and/or copying this software and/or its
 associated documentation, you agree that you have read, understood,
 and will comply with the following terms and conditions:
 .
 Permission to use, copy, modify, and distribute this software and
 its associated documentation for any purpose and without fee is
 hereby granted, provided that the above copyright notice appears in
 all copies, and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of
 Secret Labs AB or the author not be used in advertising or publicity
 pertaining to distribution of the software without specific, written
 prior permission.
 .
 SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
 TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANT-
 ABILITY AND FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR
 BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
 DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THIS SOFTWARE.
Comment:
 not included in debian package

Files: debian/* .gitignore
Copyright: 2005-2011 Steve Robbins <smr@debian.org>
           2008-2015, Alexander Bürger <acfb@users.sf.net>
License: GPL-2+

License: GPL-2+
 See /usr/share/common-licenses/GPL-2
